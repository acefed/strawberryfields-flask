# StrawberryFields Flask

- [3.0.0]
- [2.9.0] - 2024-08-10 - 9 files changed, 170 insertions(+), 95 deletions(-)
- [2.8.0] - 2024-03-10 - 2 files changed, 111 insertions(+), 41 deletions(-)
- [2.7.0] - 2024-02-04 - 6 files changed, 86 insertions(+), 37 deletions(-)
- [2.6.0] - 2023-11-11 - 9 files changed, 51 insertions(+), 41 deletions(-)
- [2.5.0] - 2023-11-05 - 11 files changed, 172 insertions(+), 85 deletions(-)
- [2.4.0] - 2023-04-16 - 6 files changed, 124 insertions(+), 72 deletions(-)
- [2.3.0] - 2022-11-27 - 8 files changed, 47 insertions(+), 23 deletions(-)
- [2.2.0] - 2022-11-20 - 4 files changed, 10 insertions(+), 8 deletions(-)
- [2.1.0] - 2022-06-26 - 4 files changed, 19 insertions(+), 30 deletions(-)
- [2.0.0] - 2022-03-13 - 4 files changed, 242 insertions(+), 39 deletions(-)
- [1.6.0] - 2021-10-11 - 1 files changed, 46 insertions(+), 2 deletions(-)
- [1.5.0] - 2021-10-01 - 3 files changed, 9 insertions(+), 2 deletions(-)
- [1.4.0] - 2021-09-25 - 5 files changed, 15 insertions(+), 10 deletions(-)
- [1.3.0] - 2021-04-26 - 3 files changed, 17 insertions(+), 6 deletions(-)
- [1.2.0] - 2021-02-24 - 5 files changed, 128 insertions(+), 126 deletions(-)
- [1.1.0] - 2021-01-26 - 1 files changed, 16 insertions(+), 15 deletions(-)
- 1.0.0 - 2021-01-10

[3.0.0]: https://gitlab.com/acefed/strawberryfields-flask/-/compare/27783966...master
[2.9.0]: https://gitlab.com/acefed/strawberryfields-flask/-/compare/6c1dd119...27783966
[2.8.0]: https://gitlab.com/acefed/strawberryfields-flask/-/compare/3151ae70...6c1dd119
[2.7.0]: https://gitlab.com/acefed/strawberryfields-flask/-/compare/3c788564...3151ae70
[2.6.0]: https://gitlab.com/acefed/strawberryfields-flask/-/compare/35ed0358...3c788564
[2.5.0]: https://gitlab.com/acefed/strawberryfields-flask/-/compare/f9ca91a3...35ed0358
[2.4.0]: https://gitlab.com/acefed/strawberryfields-flask/-/compare/1308e20e...f9ca91a3
[2.3.0]: https://gitlab.com/acefed/strawberryfields-flask/-/compare/8cbc8104...1308e20e
[2.2.0]: https://gitlab.com/acefed/strawberryfields-flask/-/compare/22ce7e4e...8cbc8104
[2.1.0]: https://gitlab.com/acefed/strawberryfields-flask/-/compare/f3050447...22ce7e4e
[2.0.0]: https://gitlab.com/acefed/strawberryfields-flask/-/compare/d7f088e4...f3050447
[1.6.0]: https://gitlab.com/acefed/strawberryfields-flask/-/compare/3cac2ee9...d7f088e4
[1.5.0]: https://gitlab.com/acefed/strawberryfields-flask/-/compare/ea18aad2...3cac2ee9
[1.4.0]: https://gitlab.com/acefed/strawberryfields-flask/-/compare/66c3db27...ea18aad2
[1.3.0]: https://gitlab.com/acefed/strawberryfields-flask/-/compare/c9e89388...66c3db27
[1.2.0]: https://gitlab.com/acefed/strawberryfields-flask/-/compare/b5fae0f1...c9e89388
[1.1.0]: https://gitlab.com/acefed/strawberryfields-flask/-/compare/ac99ba83...b5fae0f1
